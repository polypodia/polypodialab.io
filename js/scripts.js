
/**
 * Determines if HTML element is the viewport vertical range.
 * @param  {HTMLElement} element
 * @returns {boolean}
 */
const isElementInViewport = (element) => {
  const rect = element.getBoundingClientRect()

  return rect.top < (window.innerHeight ||
                     document.documentElement.clientHeight)
}

/**
 * Scroll window smoothly to the top of an HTML element's bounding rectangle
 * based on an element ID stored in the event target's `dataset.target` key.
 * @param  {Event} event
 */
const scrollTo = (event) => {
  const { target } = event.target.dataset

  event.preventDefault()

  const y = document.getElementById(target)
                    .getBoundingClientRect().top + window.pageYOffset

  window.scrollTo({top: y, behavior: 'smooth'})
}

// fix navigation bar to top of page and changes style based on
// vertical scroll position by applying a class
/**
 * On scroll events, change the style of the navbar if not scrolled to the
 * top, as well as changed highlighted navbar link based on which content
 * is visible in the viewport.
 */
const handleScroll = () => {
  const navbar = document.querySelector('nav')

  // determine the vertical scroll position of the page
  const scrollTop = document.documentElement.scrollTop ||
                    document.body.scrollTop

  // if not scrolled to the top, add CSS classes to change styles
  scrollTop > 0
    ? navbar.classList.add('navbar-scrolled')
    : navbar.classList.remove('navbar-scrolled')

  // find all navbar anchor target ids
  const targets = [...document.querySelectorAll('nav li > a')]
                              .map((element) => element.dataset.target)


  // determine which of the sections with ids in `targets` are visible in the
  // viewport
  const visibleSections = targets.flatMap((target) => {
    const element = document.getElementById(target)
    const section = element.closest('section')

    return isElementInViewport(section)
      ? element.id
      : []
  })

  // set CSS classes on the navbar anchor for the visible section, as well
  // as the rest of the sections
  const firstElement = visibleSections.slice(-1)[0]

  document.querySelectorAll('nav li > a').forEach((element) => {
    const { target } = element.dataset

    target === firstElement
      ? element.classList.add('selected')
      : element.classList.remove('selected')
  })
}

/**
 * Open or close the mobile menu whe the mobile menu icon is clicked
 * @param  {Event} event
 */
const handleMobileMenuClick = (event) => {
  const mobileMenu = document.querySelector('.nav-contents')

  // stops click event from reaching the `window` where
  // `handleWindowDefocusClick` hides the mobile menu
  event.stopPropagation()

  mobileMenu.classList.toggle('contents-visible')
}

/**
 * Hide the mobile menu contents when clicks occur outside the mobile menu.
 */
const handleWindowDefocusClick = () => {
  const mobileMenu = document.querySelector('.nav-contents')

  mobileMenu.classList.remove('contents-visible')
}


document.addEventListener('DOMContentLoaded',() => {
  // check scroll position on page load
  handleScroll()

  // set up scroll listener
  window.addEventListener('scroll', handleScroll)

  // set mobile menu click listener
  const mobileMenuButton = document.querySelector('.nav-mobile-menu-button')
  mobileMenuButton.addEventListener('click', handleMobileMenuClick)

  // set mobile menu defocus click listener
  window.addEventListener('click', handleWindowDefocusClick)

  // set up smooth scrolling for each nav anchor
  document.querySelectorAll('nav a').forEach(el => {
    el.addEventListener('click', scrollTo)
  })

  // set page to run with JS styles
  // will not run if JS is inactivated, defaulting to no-script styles
  document.querySelector('body').classList.remove('no-script')
})